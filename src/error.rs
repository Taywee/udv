use std::fmt;
use nom::error::Error as NomError;

#[derive(Debug)]
pub enum Error<I> where I: fmt::Debug {
    Parsing(NomError<I>),
}

impl<I> std::error::Error for Error<I> where I: fmt::Debug {}

impl<I> fmt::Display for Error<I> where I: fmt::Debug {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl<I> From<NomError<I>> for Error<I> where I: fmt::Debug {
    fn from(v: NomError<I>) -> Self {
        Self::Parsing(v)
    }
}
