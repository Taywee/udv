use crate::error::Error;
use std::borrow::Cow;
use std::vec::Vec;
use std::fmt::Debug;
use std::hash::Hash;
use std::io::Read;

use nom::bytes::streaming::take_till;
use nom::character::streaming::{anychar, char};
use nom::combinator::opt;
use nom::multi::many0;
use nom::sequence::preceded;
use nom::IResult;

pub const START_OF_HEADING: u8 = 0x01;
pub const START_OF_TEXT: u8 = 0x02;
pub const END_OF_TEXT: u8 = 0x03;
pub const ESCAPE: u8 = 0x1B;
pub const RECORD_SEPARATOR: u8 = 0x1E;
pub const UNIT_SEPARATOR: u8 = 0x1F;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Default)]
pub struct Unit<'a>(pub Cow<'a, str>);

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Default)]
pub struct Header<'a>(pub Vec<Unit<'a>>);

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Default)]
pub struct Record<'a>(pub Vec<Unit<'a>>);

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Default)]
pub struct Message<'a> {
    pub header: Option<Header<'a>>,
    pub records: Vec<Record<'a>>,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Default)]
pub struct Stream<'a>(pub Vec<Message<'a>>);

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct UDV {
    pub header: char,
    pub message: char,
    pub record: char,
    pub unit: char,
    pub escape: char,
    pub end_message: char,
    pub end_stream: char,
}

/// Return a default reference parser for binary data, using C0 control codes.
/// Return a default reference parser for text data.
impl Default for UDV {
    fn default() -> Self {
        UDV {
            header: '#',
            message: '>',
            record: '\n',
            unit: ',',
            escape: '\\',
            end_message: '<',
            end_stream: '!',
        }
    }
}

impl UDV {
    /// A text parser using c0 control sequences.
    pub fn new_c0_text() -> Self {
        UDV {
            header: '\u{01}',
            message: '\u{02}',
            record: '\u{1E}',
            unit: '\u{1F}',
            escape: '\u{1B}',
            end_message: '\u{03}',
            end_stream: '\u{04}',
        }
    }

    pub fn is_delimiter(self, c: char) -> bool {
        c == self.header
            || c == self.message
            || c == self.record
            || c == self.unit
            || c == self.escape
            || c == self.end_message
            || c == self.end_stream
    }

    pub fn parse_garbage(self, input: &str) -> IResult<&str, &str> {
        take_till(move |c| c == self.message || c == self.header || c == self.end_stream)(input)
    }

    /// ```
    /// use nom::{Err, Needed};
    /// let parser = udv::text::UDV::default();
    ///
    /// assert_eq!(parser.parse_escape(r#"\"#), Err(Err::Incomplete(Needed::new(1))));
    /// assert_eq!(parser.parse_escape(r#"\\"#), Ok(("", '\\')));
    /// assert_eq!(parser.parse_escape(r#"\n"#), Ok(("", 'n')));
    /// assert_eq!(parser.parse_escape("\\\nxyz"), Ok(("xyz", '\n')));
    /// ```
    pub fn parse_escape(self, input: &str) -> IResult<&str, char> {
        preceded(char(self.escape), anychar)(input)
    }

    /// ```
    /// use nom::{Err, Needed};
    /// use std::borrow::Cow;
    /// use udv::text::{Unit, UDV};
    /// let parser = UDV::default();
    ///
    /// assert_eq!(parser.parse_unit(r#",alpha,beta"#), Ok((",beta", Unit(Cow::Borrowed("alpha")))));
    /// assert_eq!(parser.parse_unit(",al\\\\pha,beta"), Ok((",beta", Unit(Cow::Owned("al\\pha".to_string())))));
    /// assert_eq!(parser.parse_unit(",al\npha,beta"), Ok(("\npha,beta", Unit(Cow::Borrowed("al")))));
    /// assert_eq!(parser.parse_unit(r#",alpha"#), Err(Err::Incomplete(Needed::new(1))));
    /// ```
    pub fn parse_unit(self, input: &str) -> IResult<&str, Unit<'_>> {
        let (input, _) = char(self.unit)(input)?;
        let (mut input, body) = take_till(move |c| self.is_delimiter(c))(input)?;
        let mut unit = Unit(Cow::Borrowed(body));
        while let (new_input, Some(escape)) = opt(move |input| self.parse_escape(input))(input)? {
            let body = unit.0.to_mut();
            body.push(escape);
            let (new_input, rest) = take_till(move |c| self.is_delimiter(c))(new_input)?;
            body.push_str(rest);
            input = new_input;
        }
        Ok((input, unit))
    }

    /// ```
    /// use nom::{Err, Needed};
    /// use nom::error::{Error, ErrorKind};
    /// use std::borrow::Cow;
    /// use udv::text::{Unit, UDV, Record};
    /// let parser = UDV::default();
    ///
    /// assert_eq!(parser.parse_record("\n,alpha,beta"), Err(Err::Incomplete(Needed::new(1))));
    /// assert_eq!(parser.parse_record("\n,alpha,beta\n"), Ok(("\n", Record(vec![Unit(Cow::Borrowed("alpha")), Unit(Cow::Borrowed("beta"))]))));
    /// assert_eq!(parser.parse_record("\n,alpha,be\\\\ta\nafter"), Ok(("\nafter", Record(vec![Unit(Cow::Borrowed("alpha")), Unit(Cow::Owned("be\\ta".to_string()))]))));
    /// assert!(matches!(parser.parse_record("#,alpha,be\\\\ta\nafter"), Err(Err::Error(Error {code: ErrorKind::Char, ..}))));
    /// ```
    pub fn parse_record(self, input: &str) -> IResult<&str, Record<'_>> {
        let (input, units) = preceded(
            char(self.record),
            many0(move |input| self.parse_unit(input)),
        )(input)?;
        Ok((input, Record(units)))
    }

    /// ```
    /// use nom::{Err, Needed};
    /// use std::borrow::Cow;
    /// use udv::text::{Unit, UDV, Header};
    /// let parser = UDV::default();
    ///
    /// assert_eq!(parser.parse_header("#,alpha,beta"), Err(Err::Incomplete(Needed::new(1))));
    /// assert_eq!(parser.parse_header("#,alpha,beta\n"), Ok(("\n", Header(vec![Unit(Cow::Borrowed("alpha")), Unit(Cow::Borrowed("beta"))]))));
    /// assert_eq!(parser.parse_header("#,alpha,be\\\\ta\nafter"), Ok(("\nafter", Header(vec![Unit(Cow::Borrowed("alpha")), Unit(Cow::Owned("be\\ta".to_string()))]))));
    /// ```
    pub fn parse_header(self, input: &str) -> IResult<&str, Header<'_>> {
        let (input, units) = preceded(
            char(self.header),
            many0(move |input| self.parse_unit(input)),
        )(input)?;
        Ok((input, Header(units)))
    }

    /// ```
    /// use nom::{Err, Needed};
    /// use std::borrow::Cow;
    /// use udv::text::{Unit, UDV, Header, Record, Message};
    /// let parser = UDV::default();
    ///
    /// assert_eq!(
    ///     parser.parse_message(">\n,alpha,beta\n,gamma,delta\n<"),
    ///     Ok(("", Message{
    ///         header: None,
    ///         records: vec![
    ///             Record(vec![Unit(Cow::Borrowed("alpha")), Unit(Cow::Borrowed("beta"))]),
    ///             Record(vec![Unit(Cow::Borrowed("gamma")), Unit(Cow::Borrowed("delta"))]),
    ///             // Trailing empty record because of newline.
    ///             Record(vec![]),
    ///         ]
    ///     }))
    /// );
    /// assert_eq!(
    ///     parser.parse_message("#,one,two>\n,alpha,beta\n,gamma,delta\n<"),
    ///     Ok(("", Message{
    ///         header: Some(Header(vec![Unit(Cow::Borrowed("one")), Unit(Cow::Borrowed("two"))])),
    ///         records: vec![
    ///             Record(vec![Unit(Cow::Borrowed("alpha")), Unit(Cow::Borrowed("beta"))]),
    ///             Record(vec![Unit(Cow::Borrowed("gamma")), Unit(Cow::Borrowed("delta"))]),
    ///             // Trailing empty record because of newline.
    ///             Record(vec![]),
    ///         ]
    ///     }))
    /// );
    /// assert_eq!(
    ///     parser.parse_message("#,one,two>\n,alpha,beta\n,gamma,delta\n"),
    ///     Err(Err::Incomplete(Needed::new(1)))
    /// );
    pub fn parse_message(self, input: &str) -> IResult<&str, Message<'_>> {
        let (input, header) = opt(move |input| self.parse_header(input))(input)?;
        let (input, _) = char(self.message)(input)?;
        let (input, records) = many0(move |input| self.parse_record(input))(input)?;
        // Convert errors to failure, since we've gotten the start of the message already, this
        // can not be correct in any branch.
        let (input, _) = match char(self.end_message)(input) {
            Err(nom::Err::Error(error)) => return Err(nom::Err::Failure(error)),
            e => e?,
        };
        Ok((input, Message { header, records }))
    }

    /// ```
    /// use nom::{Err, Needed};
    /// use std::borrow::Cow;
    /// use udv::text::{Unit, UDV, Header, Record, Message, Stream};
    /// let parser = UDV::default();
    ///
    /// assert_eq!(
    ///     parser.parse_stream(">\n,alpha,beta\n,gamma,delta\n<!"),
    ///     Ok(("", Stream(vec![
    ///         Message{
    ///             header: None,
    ///             records: vec![
    ///                 Record(vec![Unit(Cow::Borrowed("alpha")), Unit(Cow::Borrowed("beta"))]),
    ///                 Record(vec![Unit(Cow::Borrowed("gamma")), Unit(Cow::Borrowed("delta"))]),
    ///                 // Trailing empty record because of newline.
    ///                 Record(vec![]),
    ///             ]
    ///         }
    ///     ])))
    /// );
    /// assert_eq!(
    ///     parser.parse_stream("#,one,two>\n,alpha,beta\n,gamma,delta\n<!"),
    ///     Ok(("", Stream(vec![
    ///         Message{
    ///             header: Some(Header(vec![Unit(Cow::Borrowed("one")), Unit(Cow::Borrowed("two"))])),
    ///             records: vec![
    ///                 Record(vec![Unit(Cow::Borrowed("alpha")), Unit(Cow::Borrowed("beta"))]),
    ///                 Record(vec![Unit(Cow::Borrowed("gamma")), Unit(Cow::Borrowed("delta"))]),
    ///                 // Trailing empty record because of newline.
    ///                 Record(vec![]),
    ///             ]
    ///         }
    ///     ])))
    /// );
    /// assert_eq!(
    ///     parser.parse_stream(">\n,alpha,beta\n,gamma,delta\n<\ngarbage\n#,one,two>\n,alpha,beta\n,gamma,delta\n<!"),
    ///     Ok(("", Stream(vec![
    ///         Message{
    ///             header: None,
    ///             records: vec![
    ///                 Record(vec![Unit(Cow::Borrowed("alpha")), Unit(Cow::Borrowed("beta"))]),
    ///                 Record(vec![Unit(Cow::Borrowed("gamma")), Unit(Cow::Borrowed("delta"))]),
    ///                 // Trailing empty record because of newline.
    ///                 Record(vec![]),
    ///             ]
    ///         },
    ///         Message{
    ///             header: Some(Header(vec![Unit(Cow::Borrowed("one")), Unit(Cow::Borrowed("two"))])),
    ///             records: vec![
    ///                 Record(vec![Unit(Cow::Borrowed("alpha")), Unit(Cow::Borrowed("beta"))]),
    ///                 Record(vec![Unit(Cow::Borrowed("gamma")), Unit(Cow::Borrowed("delta"))]),
    ///                 // Trailing empty record because of newline.
    ///                 Record(vec![]),
    ///             ]
    ///         },
    ///     ])))
    /// );
    /// assert_eq!(
    ///     parser.parse_stream("#,one,two>\n,alpha,beta\n,gamma,delta\n<"),
    ///     Err(Err::Incomplete(Needed::new(1)))
    /// );
    pub fn parse_stream(self, input: &str) -> IResult<&str, Stream<'_>> {
        let (mut input, _) = self.parse_garbage(input)?;
        let mut messages = Vec::new();
        while let (new_input, Some(message)) = opt(move |input| self.parse_message(input))(input)? {
            messages.push(message);
            let (new_input, _) = self.parse_garbage(new_input)?;
            input = new_input;
        }
        let (input, _) = char(self.end_stream)(input)?;
        Ok((input, Stream(messages)))
    }
}

pub struct Reader<R>
where
    R: Read,
{
    udv: UDV,
    inner: R,
    buffer: String,
}

pub struct StreamReader<'a, R>
where
    R: Read,
{
    reader: &'a mut Reader<R>,
}

pub struct MessageReader<'a, R>
where
    R: Read,
{
    reader: &'a mut Reader<R>,
    header: Option<Option<Vec<String>>>,
}

impl<R> Reader<R>
where
    R: Read,
{
    pub fn new(udv: UDV, reader: R) -> Self {
        Self {
            udv,
            inner: reader,
            buffer: String::new(),
        }
    }

    pub fn stream(&mut self) -> StreamReader<'_, R> {
        StreamReader { reader: self }
    }

    pub fn message(&mut self) -> MessageReader<'_, R> {
        MessageReader { reader: self , header: None }
    }
}

impl<'a, R> MessageReader<'a, R>
where
    R: Read,
{
    pub fn header(&mut self) -> Result<&Option<Vec<String>>, Error<&'a str>> {
        if self.header.is_none() {
            loop {
                match self.reader.udv.parse_header(&self.reader.buffer) {
                    Ok((rest, header)) => {
                        let mut vec = Vec::new();
                        for unit in header.0 {
                            vec.push(unit.0.into_owned());
                        }
                        self.header = Some(Some(vec));
                        self.reader.buffer = rest.to_string();
                    }
                    Err(e) => {
                        match e {
                            nom::Err::Incomplete(e) => {
                                let needed = match e {
                                    nom::Needed::Unknown => 1,
                                    nom::Needed::Size(needed) => needed.get(),
                                };
                                let mut buf = vec![0u8; needed];
                                let read = self.reader.inner.read(
                            }
                            nom::Err::Error(_) => todo!(),
                            nom::Err::Failure(_) => todo!(),
                        }
                    }
                }
            }
        }

        Ok(self.header.as_ref().unwrap())
    }
}
